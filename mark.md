<!-- Heading -->
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

<!--Italics -->


*this text* is italic

_this text_ is italic

<!--Strong -->

**This text** is bold

# __This__ text is bold

<!--Strikrthrough -->

 ### ~~This text~~ is Strikethrough

 <!--Horizontl Rule -->

---
___

<!--Blockquote -->
>This is a blockquote

<!--Link -->

[social media](http://www.socialMedia.com)

[social media](http://www.socialMedia.com
 "jagan" )

<!--UL -->
* Item 1
* Item 2
* Item 3
    * Nested item 1
    * Nested item 2  

<!--OL -->

 # Items
1. Item 1
1. Item 2

<!--Inline Code Block-->

`<p> This is  a paragraph </p>`

<!--Image-->

![markdom Logo](https://markdown-here.com/img/icon256.png)


<!--Github Mardown -->

<!--Code Blocks-->

``` bash   
npm install

npm start

```

``` javascript
function add(num1, num2){
return  num1 +num2
}

```

<!--Table -->


| name  |  email | age  |  id | year  |
|---|---|---|---|---|
|  jagan |  jagan@gmail.com |  24 | 01  |  1998 |
|  surya |  surya@gmail.com  | 23  |  02 | 1999|

